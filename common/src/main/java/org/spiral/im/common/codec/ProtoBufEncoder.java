package org.spiral.im.common.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;
import org.spiral.im.common.bean.msg.IMMsg;

/**
 * 自定义编码器
 *
 * @author : spiral
 * @since : 2021/4/20 - 上午7:36
 */
@Slf4j
public class ProtoBufEncoder extends MessageToByteEncoder<IMMsg.Message> {
    @Override
    protected void encode(ChannelHandlerContext ctx, IMMsg.Message message, ByteBuf byteBuf) throws Exception {
        // 将对象转换为byte
        byte[] bytes = message.toByteArray();

        // 加密消息体
        //ThreeDES des = channel.channel().attr(AppAttrKeys.ENCRYPT).get();
        //byte[] encryptByte = des.encrypt(bytes);
        // 读取消息的长度
        int length = bytes.length;

        ByteBuf buf = ctx.alloc().buffer(2 + length);

        // 先将消息长度写入，也就是消息头
        buf.writeShort(length);
        // 消息体中包含我们要发送的数据
        buf.writeBytes(bytes);
        byteBuf.writeBytes(buf);

        log.debug("send " + "[remote ip:" + ctx.channel()
                                               .remoteAddress() + "][total length:" + length + "][bare length:" + message
                .getSerializedSize() + "]");

        if (buf.refCnt() > 0) {
            log.debug("释放临时缓冲");
            buf.release();
        }

    }

}
